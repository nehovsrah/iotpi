package main.java;

import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;

@WebSocket
public class ChatWebSocketHandler {

    private String sender, msg;

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        String username = "User" + OMessenger.nextUserNumber++;
        OMessenger.userUsernameMap.put(user, username);
      //  OMessenger.broadcastMessage(sender = "Server", msg = (username + " joined the chat"));
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        String username = OMessenger.userUsernameMap.get(user);
        OMessenger.userUsernameMap.remove(user);
      //  OMessenger.broadcastMessage(sender = "Server", msg = (username + " left the chat"));
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
      //  OMessenger.broadcastMessage(sender = OMessenger.userUsernameMap.get(user), msg = message);
    }

}
