package main.java;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.websocket.api.Session;
import org.json.simple.JSONObject;

import static spark.Spark.*;

public class OMessenger {

    // this map is shared between sessions and threads, so it needs to be thread-safe (http://stackoverflow.com/a/2688817)
    static Map<Session, String> userUsernameMap = new ConcurrentHashMap<>();
    static int nextUserNumber = 1; //Assign to username for next connecting user

    public OMessenger() {
        staticFiles.location("/public"); //index.html is served at localhost:4567 (default port)
        staticFiles.expireTime(6000);
        webSocket("/chat", ChatWebSocketHandler.class);
        init();
    }
    
    public void broadcastCommand(JSONObject message) {
        userUsernameMap.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
                session.getRemote().sendString(String.valueOf(message)
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
